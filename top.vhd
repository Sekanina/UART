library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;    


-- Input/output description
entity top is
    port (
        BTN0: in std_logic;     -- button 0
        BTN1: in std_logic;     -- button 1
        SW0: in std_logic;     -- switch 0  parita volba
        SW1: in std_logic;     -- switch 1  stop bit volba
        TX_LINE: out std_logic;  
        CLK: in std_logic;
        D_POS: out std_logic_vector(3 downto 0);
        D_SEG: out std_logic_vector(6 downto 0)
        
    );
--  1 MHz Clock, 9600 baud UART
-- (100000)/(9600) = 104

end top;

architecture Behavioral of top is

signal CLK_BIT : integer := 104;
signal CLK_BIT_long : integer := 208;
signal CLK_BIT_long_2 : integer := 416;
signal clk_10: std_logic := '0';
signal tmp_10: std_logic_vector(11 downto 0) := x"000";    -- hexadecimal value
signal tx_state: integer := 0;
signal Clk_Count : integer range 0 to 104 := 0;
signal Clk_Count_long : integer range 0 to 208 := 0;
signal Clk_Count_long_2 : integer range 0 to 416 := 0;
signal parity: std_logic:= '0';
signal pom : std_logic:= '0';
signal segment: std_logic_vector(1 downto 0) := "00";         -- binary value
signal data_tx: std_logic_vector(8 downto 0) := B"001010101";
type stavy is (DEFAULT, START, DATA,PARITA, STOP);
signal UART: stavy;


begin


process (CLK)
    begin
        if rising_edge(CLK) then
            tmp_10 <= tmp_10 + 1;
            if tmp_10 = x"032" then
                tmp_10 <= x"000";
                clk_10 <= not clk_10;
            end if;
        end if;
    end process;  

process (CLK)
    begin
        if rising_edge(CLK) then
            if BTN0='0' then--reset
                uart <= DEFAULT;
            else
                case uart is
                
                    when DEFAULT=>
                        TX_LINE<='1';  --klid na lince data line v 1
                        if Clk_Count < Clk_Count-1 then
                            Clk_Count <= Clk_Count + 1;
                        else
                            Clk_Count <= 0;
                            uart <= START;
                        end if;
                        
                    when START=> 
                          TX_LINE<='0';    -- data line do 0
                          if Clk_Count_long < CLK_BIT_long-1 then
                             Clk_Count_long <= Clk_Count_long + 1;
                           else
                             Clk_Count_long <= 0;
                             uart <= DATA;
                            end if;
                            
                    when DATA => --poslat data
                        TX_LINE<=data_tx(tx_state);
                        if tx_state=0 then
                            parity<=data_tx(0);
                        else 
                            pom <= parity xor data_tx(tx_state+1)xor data_tx(1);
                        end if;
                        if Clk_Count < CLK_BIT-1 then
                            Clk_Count <= Clk_Count + 1;
                        else
                            Clk_Count <= 0;
                            if tx_state <data_tx'length-1 then
                               tx_state<=tx_state+1;
                               parity <= pom;
                               uart<=DATA;
                            else 
                                tx_state<=0;
                                uart<=PARITA;
                            end if;
                        end if;
              
                    when PARITA=> -- Paritni bit(odd-lichy;even - sudy)
                        if SW0='0' then
                            TX_LINE<=parity;
                        else
                            TX_LINE<=not parity;
                        end if;
                        if Clk_Count < CLK_BIT-1 then
                            Clk_Count <= Clk_Count + 1;
                        else
                            Clk_Count <= 0;
                            pom <= '0';
                            parity <= '0';
                            uart<=STOP;
                        end if;
              
                    when STOP=> -- konec komunikace
                                -- stop bity
                        if SW1='0' then   -- 1 stop bit
                            TX_LINE<='1';
                            if Clk_Count_long < CLK_BIT_long-1 then   -- doba trvani dvou bitu
                                Clk_Count_long <= Clk_Count_long + 1;
                            else
                                Clk_Count_long <= 0;
                                uart <= DEFAULT;   
                            end if;              
           
                        else     
                            TX_LINE<='1';
                            if Clk_Count_long_2 < CLK_BIT_long_2-1 then   -- doba trvani dvou bitu
                                Clk_Count_long_2 <= Clk_Count_long_2 + 1;
                            else
                                Clk_Count_long_2 <= 0;
                                uart <= DEFAULT;  
                            end if;
          
                        end if;
                end case;
            end if;
        end if;
end process;

process (clk_10)
begin
    if clk_10 = '0' then
        D_POS <= "0111";
        if SW1='0' then
            segment<="00";
        else 
            segment<="01";
        end if;
    else
        D_POS <= "1110";
        if SW0='0' then
            segment<="10";
        else
            segment<="11";
        end if;
    end if;
end process;


with segment select                        
        D_SEG <= "1111001" when "00",-- 1 stop bit    
                 "0100100" when "01",-- 2 stop bity   
                 "1000111" when "10",-- Suda parita   
                 "0010010" when "11",-- Licha parita    
                 "1000000" when others;     

end Behavioral;
